"""[summary]
"""

from board import Board
from backtrack_algorithm import run

if __name__ == '__main__':
    print("+------------------------------------------+")
    print("|          Welcome to sudoku (^_^)         |")
    print("+------------------------------------------+")
    game_board = Board()

    game_board.insert_values()
    print("+------------------------------------------+")
    print("|                   Sudoku                 |")
    print("+------------------------------------------+")
    print(game_board)
    run(game_board.board)
    print("+------------------------------------------+")
    print("|                The Answer                |")
    print("+------------------------------------------+")
    print(game_board)
    print("+------------------------------------------+")
    print("|                 The End                  |")
    print("+------------------------------------------+")
